package com.quiz.quizmanager.client;

import com.quiz.quizmanager.viewmodel.OpentdbResponse;

public interface OpentdbClient {

  OpentdbResponse getQuestionsFromOpentdb(int amount);
}
