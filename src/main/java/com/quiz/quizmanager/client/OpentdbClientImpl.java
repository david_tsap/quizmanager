package com.quiz.quizmanager.client;

import com.quiz.quizmanager.viewmodel.OpentdbResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.http.RequestEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponents;
import org.springframework.web.util.UriComponentsBuilder;

@Service
public class OpentdbClientImpl implements OpentdbClient {

  private final Logger logger = LoggerFactory.getLogger(getClass());

  @Autowired
  protected RestTemplate restTemplate;

  @Value("${endpoint.opentdb}")
  private String fullURL;

  @Override
  public OpentdbResponse getQuestionsFromOpentdb(int amount){
    logger.debug("sending rest request to :" + fullURL);
    UriComponents url = UriComponentsBuilder
        .fromHttpUrl(fullURL).buildAndExpand(amount);
      ResponseEntity<OpentdbResponse> exchange = restTemplate.exchange(
          new RequestEntity<>( HttpMethod.GET, url.toUri()),
          new ParameterizedTypeReference<OpentdbResponse>() {
          });
      OpentdbResponse body = exchange.getBody();
      logger.debug("received response from :" + exchange );
      return body;
  }
}
