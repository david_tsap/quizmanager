package com.quiz.quizmanager.service;

import com.quiz.quizmanager.constant.StatusAnswer;
import com.quiz.quizmanager.exception.Errors;
import com.quiz.quizmanager.exception.QuizException;
import com.quiz.quizmanager.exception.QuizObjectNotFoundException;
import com.quiz.quizmanager.repository.GameQuestionMapRepoInMemory;
import com.quiz.quizmanager.repository.GameRepoInMemory;
import com.quiz.quizmanager.servicemodel.Game;
import com.quiz.quizmanager.servicemodel.Question;
import com.quiz.quizmanager.viewmodel.UserScore;
import com.quiz.quizmanager.viewmodel.UserScore.UserScoreBuilder;
import com.quiz.quizmanager.viewmodel.UserAnswer;
import com.quiz.quizmanager.viewmodel.UserResponseAnswer;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

@Service
public class GameServiceImpl implements GameService {


  @Autowired
  private GameRepoInMemory gameRepoInMemory;

  @Autowired
  private QuestionService questionService;

  @Autowired
  private GameQuestionMapRepoInMemory gameQuestionMapRepoInMemory;

  @Value("${quiz.question.count}")
  private Integer numberOfQuestionPerGame;

  @Override
  public UserResponseAnswer submitAnswer(UserAnswer userAnswer) throws QuizException {
    Game game = getGame(userAnswer.getGameId());
    Question question  = questionService.getGameQuestion(userAnswer.getGameId(),userAnswer.getQuestionId());
    UserResponseAnswer userResponseAnswer = new UserResponseAnswer();
    if(questionService.submitAnswer(question,userAnswer.getAnswerId(),userAnswer.getUserName())){
      addUserToLeaderBoard(game.getId(),userAnswer.getUserName(),question.getPoints());
      userResponseAnswer.setPointsEarned(question.getPoints());
      userResponseAnswer.setStatus(StatusAnswer.CORRECT);
    }else{
      userResponseAnswer.setPointsEarned(0);
      userResponseAnswer.setStatus(StatusAnswer.INCORRECT);
    }
    return userResponseAnswer;
  }

  @Override
  public List<UserScore> leaderBoardGame(Integer gameId) throws QuizException {
    Game gameById = getGame(gameId);
    List<UserScore> userScore = new ArrayList<>();
    gameById.getUserScore().forEach((user, score) ->{
      userScore.add(UserScoreBuilder.anUserScore()
          .withName(user)
          .withScore(score)
          .build());
    });
    return userScore;
  }

  @Override
  public Game createGame() {
    Game game  = createNewGame();
    gameRepoInMemory.saveGame(game);
    return game;
  }

  private Game getGame(Integer gameId) throws QuizException {
    Game gameById = gameRepoInMemory.getGameById(gameId);
    if(gameById == null){
      throw new QuizObjectNotFoundException("Game not found").setErrorCode(Errors.QUIZ_10001);
    }
    return gameById;
  }

  private Game createNewGame() {
    List<Question> questions = questionService.createQuestions(numberOfQuestionPerGame);
    Game game = new Game();
    game.setUserScores(new HashMap<>());
    gameQuestionMapRepoInMemory.save(game.getId(),questions.stream().map(Question::getId).collect(
        Collectors.toList()));
    return game;
  }

  private void addUserToLeaderBoard(Integer gameId, String userName, int points)
      throws QuizException {
    Game gameById = getGame(gameId);
    Map<String, Integer> userScore = gameById.getUserScore();
    if(userScore.containsKey(userName)){
      Integer oldScore = userScore.get(userName);
      userScore.put(userName,oldScore+points);
    }else{
      userScore.put(userName,points);
    }
    gameById.setUserScores(userScore);
    gameRepoInMemory.saveGame(gameById);
  }
}
