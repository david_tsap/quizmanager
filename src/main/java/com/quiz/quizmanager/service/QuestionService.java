package com.quiz.quizmanager.service;

import com.quiz.quizmanager.exception.QuizException;
import com.quiz.quizmanager.servicemodel.Question;
import java.util.List;

public interface QuestionService {

   List<Question> createQuestions(int number);

   Question getQuestion(Integer questionId) throws QuizException;

   boolean submitAnswer(Question question, Integer answerId,String userName) throws QuizException;

  Question getGameQuestion(Integer gameId, Integer questionId) throws QuizException;

  List<Question> getQuestions(List<Integer> questionsId) throws QuizException;

  List<Integer> findByGame(Integer id);
}
