package com.quiz.quizmanager.service;

import com.quiz.quizmanager.exception.QuizException;
import com.quiz.quizmanager.exception.QuizObjectNotFoundException;
import com.quiz.quizmanager.servicemodel.Game;
import com.quiz.quizmanager.viewmodel.UserScore;
import com.quiz.quizmanager.viewmodel.UserAnswer;
import com.quiz.quizmanager.viewmodel.UserResponseAnswer;
import java.util.List;

public interface GameService {

  UserResponseAnswer submitAnswer(UserAnswer userAnswer) throws QuizException;

  List<UserScore> leaderBoardGame(Integer gameId) throws QuizException;

  Game createGame();
}
