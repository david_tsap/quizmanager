package com.quiz.quizmanager.service;

import com.quiz.quizmanager.client.OpentdbClient;
import com.quiz.quizmanager.exception.Errors;
import com.quiz.quizmanager.exception.QuizException;
import com.quiz.quizmanager.exception.QuizObjectNotFoundException;
import com.quiz.quizmanager.mapper.OpentDbQuestionMapper;
import com.quiz.quizmanager.repository.GameQuestionMapRepoInMemory;
import com.quiz.quizmanager.repository.QuestionRepoInMemory;
import com.quiz.quizmanager.servicemodel.Answer;
import com.quiz.quizmanager.viewmodel.OpentdbResponse;
import com.quiz.quizmanager.servicemodel.Question;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class QuestionServiceImpl implements QuestionService{

  @Autowired
  OpentdbClient opentdbClient;

  @Autowired
  OpentDbQuestionMapper opentDbQuestionMapper;

  @Autowired
  QuestionRepoInMemory questionRepoInMemory;

  @Autowired
  private GameQuestionMapRepoInMemory gameQuestionMapRepoInMemory;

  @Override
  public List<Question> createQuestions(int number) {
    OpentdbResponse questionsFromOpentdb = opentdbClient.getQuestionsFromOpentdb(number);
    List<Question> questions = opentDbQuestionMapper.createQuestions(questionsFromOpentdb);
    questionRepoInMemory.saveQuestion(questions);
    return questions;
  }

  @Override
  public Question getQuestion(Integer questionId) throws QuizException {
    Question question = questionRepoInMemory.getQuestionById(questionId);
    if(question == null){
      throw new QuizObjectNotFoundException("Question not found").setErrorCode(Errors.QUIZ_10002);
    }
    return question;
  }

  @Override
  public boolean submitAnswer(Question question, Integer answerId,String userName)
      throws QuizException {
    //validate answerExist
    validateAnswerExist(question,answerId);
    //validate user not exist
    validateUserNotAnswerQuestion(question,userName);
    Answer correctAnswers = question.getCorrectAnswers();
    addUserToQuestion(question,userName);
    if(correctAnswers.getId() == answerId){
      return true;
    }else{
      return false;
    }
  }

  private void validateUserNotAnswerQuestion(Question question, String userName)
      throws QuizException {
    if(question.getUserAnswer() !=null && question.getUserAnswer().contains(userName)){
      throw new QuizException("user already answer the question").setErrorCode(Errors.QUIZ_10004);
    }
  }


  @Override
  public Question getGameQuestion(Integer gameId, Integer questionId)
      throws QuizException {
    List<Question> gameQuestions = gameQuestionMapRepoInMemory.findByGame(gameId);
    Optional<Question> optionalQuestion = gameQuestions.stream()
        .filter(question -> question.getId().equals(questionId))
        .findFirst();
    if(!optionalQuestion.isPresent()){
      throw new QuizObjectNotFoundException("Question not found in the Game").setErrorCode(Errors.QUIZ_10002);
    }
    Question question = optionalQuestion.get();
    return question;
  }

  @Override
  public List<Question> getQuestions(List<Integer> questionsId) throws QuizException {
    List<Question> questions = new ArrayList<>();
    for(Integer questionId : questionsId){
      questions.add(getQuestion(questionId));
    }
    return questions;
  }

  @Override
  public List<Integer> findByGame(Integer id) {
    return gameQuestionMapRepoInMemory.findByGameGetIds(id);
  }


  private void validateAnswerExist(Question question, Integer answerId)
      throws QuizException {
    Optional<Answer> answerOptional = question.getIncorrectAnswer().stream()
        .filter(answer -> answer.getId() == answerId).findFirst();// found answer in wrong answer
    if(!answerOptional.isPresent() && question.getCorrectAnswers().getId() != answerId){
      throw new QuizObjectNotFoundException("Answer not found").setErrorCode(Errors.QUIZ_10003);
    }
  }

  private void addUserToQuestion(Question question,String userName){
    if(question.getUserAnswer() == null){
      question.setUserAnswer(new ArrayList<>());
    }
    question.getUserAnswer().add(userName);
    questionRepoInMemory.saveQuestion(Arrays.asList(question));
  }


}
