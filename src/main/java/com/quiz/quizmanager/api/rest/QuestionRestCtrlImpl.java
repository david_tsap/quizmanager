package com.quiz.quizmanager.api.rest;

import com.quiz.quizmanager.exception.QuizException;
import com.quiz.quizmanager.service.QuestionService;
import com.quiz.quizmanager.servicemodel.Question;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class QuestionRestCtrlImpl implements QuestionRestCtrl{

  @Autowired
  private QuestionService questionService;
  private final Logger logger = LoggerFactory.getLogger(getClass());

  @Override
  @GetMapping("/api/question/{questionId}")
  public ResponseEntity<Question> getQuestion(@PathVariable Integer questionId)
      throws QuizException {
    logger.info("received getQuestion request for : " + questionId.toString());
    Question question = questionService.getQuestion(questionId);
    logger.info("response for getQuestion: " + question.toString());
    return new ResponseEntity<>(question, HttpStatus.OK);
  }

  @Override
  @PostMapping("/api/questions")
  public ResponseEntity<List<Question>> getQuestions(@RequestBody List<Integer> questionsId)
      throws QuizException {
    logger.info("received getQuestions request for : " + questionsId.toString());
    List<Question> questions = questionService.getQuestions(questionsId);
    logger.info("response for getQuestions: " + questions.toString());
    return new ResponseEntity<>(questions, HttpStatus.OK);
  }
}
