package com.quiz.quizmanager.api.rest;

import com.quiz.quizmanager.exception.QuizException;
import com.quiz.quizmanager.mapper.GameMapper;
import com.quiz.quizmanager.service.GameService;
import com.quiz.quizmanager.viewmodel.Game;
import com.quiz.quizmanager.viewmodel.UserScore;
import com.quiz.quizmanager.viewmodel.UserAnswer;
import com.quiz.quizmanager.viewmodel.UserResponseAnswer;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class GameRestCtrlImpl implements GameRestCtrl{

  @Autowired
  private GameService gameService;
  private final Logger logger = LoggerFactory.getLogger(getClass());

  @Autowired
  private GameMapper gameMapper;


  @Override
  @PostMapping("/api/answer")
  public ResponseEntity<UserResponseAnswer> submitAnswer(@RequestBody UserAnswer userAnswer)
      throws QuizException {
    logger.info("received submitAnswer request : " + userAnswer.toString());
    UserResponseAnswer userResponseAnswer = gameService.submitAnswer(userAnswer);
    logger.info("response for submitAnswer: " + userResponseAnswer.toString());
    return new ResponseEntity<>(userResponseAnswer, HttpStatus.OK);
  }

  @Override
  @GetMapping("/api/leaderboard/{gameId}")
  public ResponseEntity<List<UserScore>> leaderBoardGame(@PathVariable Integer gameId)
      throws QuizException {
    logger.info("received leaderBoardGame request for: " + gameId);
    List<UserScore> userScores = gameService.leaderBoardGame(gameId);
    logger.info("response for leaderBoardGame: " + userScores.toString());
    return new ResponseEntity<>(userScores, HttpStatus.OK);
  }

  @Override
  @PostMapping("/api/creategame")
  public ResponseEntity<Game> createGame() {
    logger.info("received createGame request");
    com.quiz.quizmanager.servicemodel.Game game = gameService.createGame();
    Game gameViewModel  = gameMapper.mapGame(game);
    logger.info("response for createGame: " + game.toString());
    return new ResponseEntity<>(gameViewModel, HttpStatus.OK);
  }
}
