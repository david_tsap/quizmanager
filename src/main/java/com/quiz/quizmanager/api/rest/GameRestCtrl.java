package com.quiz.quizmanager.api.rest;

import com.quiz.quizmanager.exception.QuizException;
import com.quiz.quizmanager.viewmodel.Game;
import com.quiz.quizmanager.viewmodel.UserScore;
import com.quiz.quizmanager.viewmodel.UserAnswer;
import com.quiz.quizmanager.viewmodel.UserResponseAnswer;
import java.util.List;
import org.springframework.http.ResponseEntity;

public interface GameRestCtrl {

  ResponseEntity<UserResponseAnswer> submitAnswer(UserAnswer userAnswer)
      throws QuizException;

  ResponseEntity<List<UserScore>> leaderBoardGame(Integer gameId)
      throws QuizException;

  ResponseEntity<Game> createGame();

}
