package com.quiz.quizmanager.api.rest;

import com.quiz.quizmanager.exception.QuizException;
import com.quiz.quizmanager.servicemodel.Question;
import java.util.List;
import org.springframework.http.ResponseEntity;

public interface QuestionRestCtrl {

   ResponseEntity<Question> getQuestion(Integer questionId) throws QuizException;

   ResponseEntity<List<Question>> getQuestions(List<Integer> questionsId) throws QuizException;
}
