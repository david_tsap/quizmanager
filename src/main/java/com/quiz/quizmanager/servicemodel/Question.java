package com.quiz.quizmanager.servicemodel;

import java.util.List;
import java.util.Random;

public class Question {

  private Integer id;
  private String question;
  private Answer correctAnswers;
  private List<Answer> incorrectAnswer;
  private int points;
  List<String> userAnswer;

  public Question(){
    Random random = new Random();
    this.id = random.ints(1000000,9999999).findFirst().getAsInt();
  }

  public List<String> getUserAnswer() {
    return userAnswer;
  }

  public void setUserAnswer(List<String> userAnswer) {
    this.userAnswer = userAnswer;
  }

  public Integer getId() {
    return id;
  }

  public void setId(Integer id) {
    this.id = id;
  }

  public String getQuestion() {
    return question;
  }

  public void setQuestion(String question) {
    this.question = question;
  }

  public Answer getCorrectAnswers() {
    return correctAnswers;
  }

  public void setCorrectAnswers(Answer correctAnswers) {
    this.correctAnswers = correctAnswers;
  }

  public List<Answer> getIncorrectAnswer() {
    return incorrectAnswer;
  }

  public void setIncorrectAnswer(List<Answer> incorrectAnswer) {
    this.incorrectAnswer = incorrectAnswer;
  }

  public int getPoints() {
    return points;
  }

  public void setPoints(int points) {
    this.points = points;
  }

  @Override
  public String toString() {
    return "Question{" +
        "id=" + id +
        ", question='" + question + '\'' +
        ", correctAnswers=" + correctAnswers +
        ", incorrectAnswer=" + incorrectAnswer +
        ", points=" + points +
        ", userAnswer=" + userAnswer +
        '}';
  }
}
