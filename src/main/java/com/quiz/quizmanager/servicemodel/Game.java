package com.quiz.quizmanager.servicemodel;

import java.util.Map;
import java.util.Random;

public class Game {

  private Integer id;
  Map<String ,Integer> userScore;

  public Game(){
    Random random = new Random();
    this.id = random.ints(1000000,9999999).findFirst().getAsInt();
  }
  public Integer getId() {
    return id;
  }

  public void setId(Integer id) {
    this.id = id;
  }


  public Map<String, Integer> getUserScore() {
    return userScore;
  }

  public void setUserScores(Map<String, Integer> userScore) {
    this.userScore = userScore;
  }

  @Override
  public String toString() {
    return "Game{" +
        "id=" + id +
        ", userScore=" + userScore +
        '}';
  }
}
