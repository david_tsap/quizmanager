package com.quiz.quizmanager.servicemodel;

import java.util.Random;

public class Answer {

  private String answer;
  private int id;

  public Answer(){
    Random random = new Random();
    this.id = random.ints(1000000,9999999).findFirst().getAsInt();
  }
  public String getAnswer() {
    return answer;
  }

  public void setAnswer(String answer) {
    this.answer = answer;
  }

  public int getId() {
    return id;
  }

  public void setId(int id) {
    this.id = id;
  }

  @Override
  public String toString() {
    return "Answer{" +
        "answer='" + answer + '\'' +
        ", id=" + id +
        '}';
  }
}
