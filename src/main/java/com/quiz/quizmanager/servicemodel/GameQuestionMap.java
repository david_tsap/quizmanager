package com.quiz.quizmanager.servicemodel;

import java.util.Random;

public class GameQuestionMap {

  private Integer id;
  private Integer gameId;
  private Integer questionId;

  public GameQuestionMap() {
    Random random = new Random();
    this.id = random.ints(1000000,9999999).findFirst().getAsInt();
  }

  public Integer getGameId() {
    return gameId;
  }

  public void setGameId(Integer gameId) {
    this.gameId = gameId;
  }

  public Integer getQuestionId() {
    return questionId;
  }

  public void setQuestionId(Integer questionId) {
    this.questionId = questionId;
  }

  public Integer getId() {
    return id;
  }

  public void setId(Integer id) {
    this.id = id;
  }

  @Override
  public String toString() {
    return "GameQuestionMap{" +
        "id=" + id +
        ", gameId=" + gameId +
        ", questionId=" + questionId +
        '}';
  }
}
