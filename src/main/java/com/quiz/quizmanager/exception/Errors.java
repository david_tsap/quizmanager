package com.quiz.quizmanager.exception;


public enum Errors {
  QUIZ_10000("QUIZ_10000", "internal error"),
  QUIZ_10001("QUIZ_10001", "Game not found"),
  QUIZ_10002("QUIZ_10002", "Question not found"),
  QUIZ_10003("QUIZ_10003", "Answer not found"),
  QUIZ_10004("QUIZ_10004", "user already answer question");



  private String code;
  private String message;

  Errors(String code, String message) {
    this.code = code;
    this.message = message;
  }


  public String code() {
    return this.code;
  }

  public String message() {
    return this.message;
  }

}
