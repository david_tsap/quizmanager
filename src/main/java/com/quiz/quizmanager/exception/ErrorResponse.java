package com.quiz.quizmanager.exception;

public class ErrorResponse {

  private String code;
  private String message;

  public String getCode() {
    return code;
  }

  public void setCode(String code) {
    this.code = code;
  }

  public String getMessage() {
    return message;
  }

  public void setMessage(String message) {
    this.message = message;
  }


  public static final class ErrorResponseBuilder {

    private String code;
    private String message;

    private ErrorResponseBuilder() {
    }

    public static ErrorResponseBuilder anErrorResponse() {
      return new ErrorResponseBuilder();
    }

    public ErrorResponseBuilder withCode(String code) {
      this.code = code;
      return this;
    }

    public ErrorResponseBuilder withMessage(String message) {
      this.message = message;
      return this;
    }

    public ErrorResponse build() {
      ErrorResponse errorResponse = new ErrorResponse();
      errorResponse.setCode(code);
      errorResponse.setMessage(message);
      return errorResponse;
    }
  }
}
