package com.quiz.quizmanager.exception;

public class QuizObjectNotFoundException extends QuizException{

  public QuizObjectNotFoundException(String massage) {
    super(massage);
  }
}
