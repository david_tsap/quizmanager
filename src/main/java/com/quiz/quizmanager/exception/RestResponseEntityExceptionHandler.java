package com.quiz.quizmanager.exception;
import com.quiz.quizmanager.exception.ErrorResponse.ErrorResponseBuilder;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@ControllerAdvice
public class RestResponseEntityExceptionHandler {

    @ExceptionHandler(value
        = { QuizException.class})
    protected ResponseEntity<ErrorResponse> handleCalcException(
        QuizException ex) {
      Errors bodyOfResponse = ex.getErrorCode();
      ErrorResponse errorResponse = ErrorResponseBuilder.anErrorResponse()
          .withCode(bodyOfResponse.code())
          .withMessage(bodyOfResponse.message())
          .build();
      return new ResponseEntity<>(errorResponse, HttpStatus.BAD_REQUEST);
    }


}