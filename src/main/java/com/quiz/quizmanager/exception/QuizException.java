package com.quiz.quizmanager.exception;

public class QuizException extends  Exception{
  protected Errors errorCode;

  public QuizException(String massage) { super(massage);}

  public QuizException setErrorCode(Errors errorCode) {
    this.errorCode = errorCode;
    return this;
  }

  public Errors getErrorCode() {
    return errorCode;
  }
}
