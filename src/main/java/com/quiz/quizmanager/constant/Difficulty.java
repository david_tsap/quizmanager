package com.quiz.quizmanager.constant;


public enum Difficulty {
  EASY(25),
  MEDIUM(50),
  HARD(100),
  UNKNOWN(50);

  private int score;

  Difficulty(int score){ this.score=score; }

  public static Difficulty fromString(String text) {
    Difficulty difficulty;
    try {
       difficulty = Enum.valueOf(Difficulty.class, text);
    }
    catch(Exception ex){
      difficulty = Difficulty.UNKNOWN;
    }
    return difficulty;
  }

  public int getScore() {
    return score;
  }
}
