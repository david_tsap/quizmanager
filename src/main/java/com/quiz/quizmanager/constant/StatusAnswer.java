package com.quiz.quizmanager.constant;

public enum StatusAnswer {
  CORRECT,
  INCORRECT;
}
