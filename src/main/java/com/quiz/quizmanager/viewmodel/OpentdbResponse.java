package com.quiz.quizmanager.viewmodel;

import com.quiz.quizmanager.viewmodel.Results;
import java.util.List;

public class OpentdbResponse {

    private int responseCode;
    private List<Results> results;

    public int getResponseCode() {
        return responseCode;
    }

    public void setResponseCode(int responseCode) {
        this.responseCode = responseCode;
    }

    public List<Results> getResults() {
        return results;
    }

    public void setResults(List<Results> results) {
        this.results = results;
    }

    @Override
    public String toString() {
        return "OpentdbResponse{" +
            "responseCode=" + responseCode +
            ", results=" + results +
            '}';
    }
}
