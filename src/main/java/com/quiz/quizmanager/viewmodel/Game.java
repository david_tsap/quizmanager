package com.quiz.quizmanager.viewmodel;

import java.util.List;
import java.util.Map;
import java.util.Random;

public class Game {

  private Integer id;
  List<Integer> questionsIds;
  Map<String ,Integer> userScore;

  public Game(){
    Random random = new Random();
    this.id = random.ints(1000000,9999999).findFirst().getAsInt();
  }
  public Integer getId() {
    return id;
  }

  public void setId(Integer id) {
    this.id = id;
  }

  public List<Integer> getQuestionsIds() {
    return questionsIds;
  }

  public void setQuestionsIds(List<Integer> questionsIds) {
    this.questionsIds = questionsIds;
  }

  public Map<String, Integer> getUserScore() {
    return userScore;
  }

  public void setUserScores(Map<String, Integer> userScore) {
    this.userScore = userScore;
  }

  @Override
  public String toString() {
    return "Game{" +
        "id=" + id +
        ", userScore=" + userScore +
        '}';
  }


  public static final class GameBuilder {

    List<Integer> questionsIds;
    Map<String ,Integer> userScore;
    private Integer id;

    private GameBuilder() {
    }

    public static GameBuilder aGame() {
      return new GameBuilder();
    }

    public GameBuilder withId(Integer id) {
      this.id = id;
      return this;
    }

    public GameBuilder withQuestionsIds(List<Integer> questionsIds) {
      this.questionsIds = questionsIds;
      return this;
    }

    public GameBuilder withUserScore(Map<String, Integer> userScore) {
      this.userScore = userScore;
      return this;
    }

    public Game build() {
      Game game = new Game();
      game.setId(id);
      game.setQuestionsIds(questionsIds);
      game.userScore = this.userScore;
      return game;
    }
  }
}
