package com.quiz.quizmanager.viewmodel;

public class UserScore {

  private String name;
  private int score;

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public int getScore() {
    return score;
  }

  public void setScore(int score) {
    this.score = score;
  }

  @Override
  public String toString() {
    return "UserScore{" +
        "name='" + name + '\'' +
        ", score=" + score +
        '}';
  }

  public static final class UserScoreBuilder {

    private String name;
    private int score;

    private UserScoreBuilder() {
    }

    public static UserScoreBuilder anUserScore() {
      return new UserScoreBuilder();
    }

    public UserScoreBuilder withName(String name) {
      this.name = name;
      return this;
    }

    public UserScoreBuilder withScore(int score) {
      this.score = score;
      return this;
    }

    public UserScore build() {
      UserScore userScore = new UserScore();
      userScore.setName(name);
      userScore.setScore(score);
      return userScore;
    }
  }
}
