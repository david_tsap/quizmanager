package com.quiz.quizmanager.viewmodel;

import com.quiz.quizmanager.constant.StatusAnswer;

public class UserResponseAnswer {

  private StatusAnswer status;
  private Integer pointsEarned;

  public StatusAnswer getStatus() {
    return status;
  }

  public void setStatus(StatusAnswer status) {
    this.status = status;
  }

  public Integer getPointsEarned() {
    return pointsEarned;
  }

  public void setPointsEarned(Integer pointsEarned) {
    this.pointsEarned = pointsEarned;
  }

  @Override
  public String toString() {
    return "UserResponseAnswer{" +
        "status=" + status +
        ", pointsEarned=" + pointsEarned +
        '}';
  }
}
