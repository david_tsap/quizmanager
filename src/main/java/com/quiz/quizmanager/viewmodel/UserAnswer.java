package com.quiz.quizmanager.viewmodel;

public class UserAnswer {

  private String userName;
  private Integer gameId;
  private Integer answerId;
  private Integer questionId;

  public String getUserName() {
    return userName;
  }

  public void setUserName(String userName) {
    this.userName = userName;
  }

  public Integer getGameId() {
    return gameId;
  }

  public void setGameId(Integer gameId) {
    this.gameId = gameId;
  }

  public Integer getAnswerId() {
    return answerId;
  }

  public void setAnswerId(Integer answerId) {
    this.answerId = answerId;
  }

  public Integer getQuestionId() {
    return questionId;
  }

  public void setQuestionId(Integer questionId) {
    this.questionId = questionId;
  }

  @Override
  public String toString() {
    return "UserAnswer{" +
        "userName='" + userName + '\'' +
        ", gameId=" + gameId +
        ", answerId=" + answerId +
        ", questionId=" + questionId +
        '}';
  }
}
