package com.quiz.quizmanager.mapper;

import com.quiz.quizmanager.service.QuestionService;
import com.quiz.quizmanager.viewmodel.Game;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class GameMapper {

  @Autowired
  QuestionService questionService;

  public Game mapGame(com.quiz.quizmanager.servicemodel.Game game) {
    List<Integer> questionId = questionService.findByGame(game.getId());
    return Game.GameBuilder.aGame()
        .withId(game.getId())
        .withUserScore(game.getUserScore())
        .withQuestionsIds(questionId)
        .build();
  }
}
