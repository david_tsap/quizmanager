package com.quiz.quizmanager.mapper;

import com.quiz.quizmanager.constant.Difficulty;
import com.quiz.quizmanager.servicemodel.Answer;
import com.quiz.quizmanager.viewmodel.OpentdbResponse;
import com.quiz.quizmanager.servicemodel.Question;
import com.quiz.quizmanager.viewmodel.Results;
import java.util.ArrayList;
import java.util.List;
import org.springframework.stereotype.Service;

@Service
public class OpentDbQuestionMapper {

  public List<Question> createQuestions(OpentdbResponse response){
    List<Results> results = response.getResults();
    List<Question> questions = new ArrayList<>();
    for(Results result : results){
      Question question = createQuestionFromOpentdbResult(result);
      questions.add(question);
    }
    return questions;
  }

  private Question createQuestionFromOpentdbResult (Results result){
    Question question = new Question();
    question.setQuestion(result.getQuestion());
    question.setPoints(Difficulty.fromString(result.getDifficulty().toUpperCase()).getScore());
    List<Answer> incorrectAnswers =  new ArrayList<>();
    for(String answer: result.getIncorrectAnswers()){
      Answer answer1 = new Answer();
      answer1.setAnswer(answer);
      incorrectAnswers.add(answer1);
    }
    question.setIncorrectAnswer(incorrectAnswers);
    Answer answer = new Answer();
    answer.setAnswer(result.getCorrectAnswer());
    question.setCorrectAnswers(answer);
    return question;
  }
}
