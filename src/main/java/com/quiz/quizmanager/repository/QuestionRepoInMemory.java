package com.quiz.quizmanager.repository;

import com.quiz.quizmanager.servicemodel.Question;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.springframework.stereotype.Service;

@Service
public class QuestionRepoInMemory {

  Map<Integer, Question> questionsRepo = new HashMap<>();

  public void saveQuestion(List<Question> questions) {
    for(Question question : questions){
      questionsRepo.put(question.getId(),question);
    }
  }

  public Question getQuestionById(Integer id){
    return questionsRepo.get(id);
  }

  public List<Question> getQuestionByIds(List<Integer> ids){
    List<Question> questions = new ArrayList<>();
    for(Integer id : ids){
      Question question = questionsRepo.get(id);
      if(question != null){
        questions.add(question);
      }
    }
    return questions;
  }

}
