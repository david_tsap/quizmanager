package com.quiz.quizmanager.repository;

import com.quiz.quizmanager.servicemodel.Game;
import java.util.HashMap;
import java.util.Map;
import org.springframework.stereotype.Service;

@Service
public class GameRepoInMemory {

  Map<Integer, Game> games = new HashMap<>();

  public void saveGame(Game game) {
    games.put(game.getId(),game);
  }

  public Game getGameById(Integer id){
    return games.get(id);
  }
}
