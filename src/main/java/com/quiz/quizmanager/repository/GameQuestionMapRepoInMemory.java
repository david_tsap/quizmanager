package com.quiz.quizmanager.repository;

import com.quiz.quizmanager.servicemodel.GameQuestionMap;
import com.quiz.quizmanager.servicemodel.Question;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class GameQuestionMapRepoInMemory {

  @Autowired
  QuestionRepoInMemory questionRepoInMemory;

  Map<Integer,GameQuestionMap> gameQuestionMap = new HashMap<>();

  public List<Question> findByGame(Integer gameId){
    List<Integer> byGameGetIds = findByGameGetIds(gameId);
    return questionRepoInMemory.getQuestionByIds(byGameGetIds);
  }

  public List<Integer> findByGameGetIds(Integer gameId){
    return gameQuestionMap.entrySet().stream()
        .filter(gameQuestionMap -> gameQuestionMap.getValue().getGameId().equals(gameId))
        .map(gameQuestionMap -> gameQuestionMap.getValue().getQuestionId())
        .collect(Collectors.toList());
  }

  public void save(Integer gameId, List<Integer> questionsIds) {
    for(Integer questionId : questionsIds){
      GameQuestionMap gameQuestionMap = new GameQuestionMap();
      gameQuestionMap.setGameId(gameId);
      gameQuestionMap.setQuestionId(questionId);
      this.gameQuestionMap.put(gameQuestionMap.getId(),gameQuestionMap);
    }
  }
}
