package com.quiz.quizmanager;

import com.quiz.quizmanager.constant.StatusAnswer;
import com.quiz.quizmanager.exception.ErrorResponse;
import com.quiz.quizmanager.exception.Errors;
import com.quiz.quizmanager.viewmodel.Game;
import com.quiz.quizmanager.viewmodel.UserScore;
import com.quiz.quizmanager.viewmodel.UserAnswer;
import com.quiz.quizmanager.viewmodel.UserResponseAnswer;
import java.util.List;
import java.util.Optional;
import org.junit.Test;

public class GameTest extends QuizManagerBaseTests{

  @Test
  public void createGame_submitCorrectAnswer_AddToLeaderBoard(){
    //create Game
    Game game = createGame();
    String userName = "user1";
    //create correct Answer
    UserAnswer correctAnswer = createCorrectAnswer(game,userName);
    UserResponseAnswer answer = answer(correctAnswer);
    assertEquals(answer.getStatus(), StatusAnswer.CORRECT);
    //get leaderBoard
    List<UserScore> leaderboard = getLeaderboard(game.getId());
    //verify user was added
    assertTrue("at least on user should be in leaderboard",leaderboard.size()>0);
    Optional<UserScore> optionalUserScore = leaderboard.stream()
        .filter(userScore -> userScore.getName().equals(userName))
        .findFirst();
    assertTrue("correct answer did not added",optionalUserScore.isPresent());

  }

  @Test
  public void createGame_submitWrongAnswer_NotAddedToLeaderBoard(){
    //create Game
    Game game = createGame();
    String userName = "user1";
    //create wrong Answer
    UserAnswer wrongAnswer = createWrongAnswer(game,userName);
    UserResponseAnswer answer = answer(wrongAnswer);
    assertEquals(answer.getStatus(),StatusAnswer.INCORRECT);
    //get leaderBoard
    List<UserScore> leaderboard = getLeaderboard(game.getId());
    //verify user not added
    assertTrue("not user should be in the leaderboard",leaderboard.size() == 0);
  }

  @Test
  public void createGame_createUnknownQuestion_getException(){
    //create game
    Game game = createGame();
    String userName = "user1";
    //create unknown question
    UserAnswer unknownQuestion = createUnknownQuestion(game,userName);
    //get correct error code
    ErrorResponse answer = errorAnswer(unknownQuestion);
    assertEquals("expected error QUIZ_10002,Question not found" ,answer.getCode(), Errors.QUIZ_10002.code());
  }

  @Test
  public void createGame_submitAnswer_getExceptionGameNotFound(){
    //create game
    Game game = createGame();
    String userName = "user1";
    //create correct question
    UserAnswer unknownQuestion = createCorrectAnswer(game,userName);
    unknownQuestion.setGameId(unknownQuestion.getGameId()+1); // change game ID
    //get correct error code
    ErrorResponse answer = errorAnswer(unknownQuestion);
    assertEquals("expected error QUIZ_10001,Question not found" ,answer.getCode(), Errors.QUIZ_10001.code());
  }

  @Test
  public void createGame_getLeaderBoardGame_getExceptionGameNotFound(){
    //create game
    Game game = createGame();
    String userName = "user1";
    //create unknown question
    ErrorResponse leaderboardError = getLeaderboardError(game.getId() +1);
    //get correct error code
    assertEquals("expected error QUIZ_10001,Question not found" ,leaderboardError.getCode(), Errors.QUIZ_10001.code());
  }

}
