package com.quiz.quizmanager;

import com.quiz.quizmanager.exception.ErrorResponse;
import com.quiz.quizmanager.viewmodel.Game;
import com.quiz.quizmanager.servicemodel.Question;
import com.quiz.quizmanager.viewmodel.UserScore;
import com.quiz.quizmanager.viewmodel.UserAnswer;
import com.quiz.quizmanager.viewmodel.UserResponseAnswer;
import java.util.List;
import java.util.Random;
import javax.annotation.PostConstruct;
import junit.framework.TestCase;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.http.RequestEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.web.util.UriComponents;
import org.springframework.web.util.UriComponentsBuilder;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.DEFINED_PORT)
public abstract class QuizManagerBaseTests extends TestCase {

	@Autowired
	protected TestRestTemplate restTemplate ;

	private final Logger logger = LoggerFactory.getLogger(getClass());

	private String createGameURL;

	private String leaderBoardUrl;

	private String answerUrl;

	private String questionUrl;


	@Value("${quiz.manager.host}")
	String host;
	@Value("${quiz.manager.port}")
	String port;

	@PostConstruct
	public void init(){
		createGameURL = host + ":" + port + "/api/creategame";
		leaderBoardUrl= host + ":" + port + "/api/leaderboard/{gameId}";
		answerUrl = host + ":" + port + "/api/answer";
		questionUrl =  host + ":" + port + "/api/question/{questionId}";
	}


	protected Game createGame(){
		logger.debug("sending rest request to :" + createGameURL );
		UriComponents url = UriComponentsBuilder
				.fromHttpUrl(createGameURL).buildAndExpand();
		ResponseEntity<Game> exchange = restTemplate.exchange(
				new RequestEntity<>(HttpMethod.POST, url.toUri()),
				new ParameterizedTypeReference<Game>() {
				});
		Game game = exchange.getBody();
		logger.debug("received response from :" + exchange );
		return game;
	}

	protected UserResponseAnswer answer(UserAnswer userAnswer){
		logger.debug("sending rest request to :" + answerUrl );
		UriComponents url = UriComponentsBuilder
				.fromHttpUrl(answerUrl).buildAndExpand();
		ResponseEntity<UserResponseAnswer> exchange = restTemplate.exchange(
				new RequestEntity<>(userAnswer,HttpMethod.POST, url.toUri()),
				new ParameterizedTypeReference<UserResponseAnswer>() {
				});
		UserResponseAnswer userResponseAnswer = exchange.getBody();
		logger.debug("received response from :" + exchange );
		return userResponseAnswer;

	}

	protected ErrorResponse errorAnswer(UserAnswer userAnswer){
		logger.debug("sending rest request to :" + answerUrl );
		UriComponents url = UriComponentsBuilder
				.fromHttpUrl(answerUrl).buildAndExpand();
			ResponseEntity<ErrorResponse> exchange = restTemplate.exchange(
					new RequestEntity<>(userAnswer,HttpMethod.POST, url.toUri()),
					new ParameterizedTypeReference<ErrorResponse>() {
					});
		ErrorResponse userResponseAnswer = exchange.getBody();
			logger.debug("received response from :" + exchange );
			return userResponseAnswer;

	}

	protected List<UserScore> getLeaderboard(int gameId){
		logger.debug("sending rest request to :" + leaderBoardUrl );
		UriComponents url = UriComponentsBuilder
				.fromHttpUrl(leaderBoardUrl).buildAndExpand(gameId);
		ResponseEntity<List<UserScore>> exchange = restTemplate.exchange(
				new RequestEntity<>(HttpMethod.GET, url.toUri()),
				new ParameterizedTypeReference<List<UserScore>>() {
				});
		List<UserScore> body = exchange.getBody();
		logger.debug("received response from :" + exchange );
		return body;
	}

	protected ErrorResponse getLeaderboardError(int gameId){
		logger.debug("sending rest request to :" + leaderBoardUrl );
		UriComponents url = UriComponentsBuilder
				.fromHttpUrl(leaderBoardUrl).buildAndExpand(gameId);
		ResponseEntity<ErrorResponse> exchange = restTemplate.exchange(
				new RequestEntity<>(HttpMethod.GET, url.toUri()),
				new ParameterizedTypeReference<ErrorResponse>() {
				});
		ErrorResponse body = exchange.getBody();
		logger.debug("received response from :" + exchange );
		return body;
	}

	protected Question getQuestion(int questionId){
		logger.debug("sending rest request to :" + questionUrl );
		UriComponents url = UriComponentsBuilder
				.fromHttpUrl(questionUrl).buildAndExpand(questionId);
		ResponseEntity<Question> exchange = restTemplate.exchange(
				new RequestEntity<>(HttpMethod.GET, url.toUri()),
				new ParameterizedTypeReference<Question>() {
				});
		Question question = exchange.getBody();
		logger.debug("received response from :" + exchange );
		return question;
	}

	protected ErrorResponse getQuestionError(int questionId){
		logger.debug("sending rest request to :" + questionUrl );
		UriComponents url = UriComponentsBuilder
				.fromHttpUrl(questionUrl).buildAndExpand(questionId);
		ResponseEntity<ErrorResponse> exchange = restTemplate.exchange(
				new RequestEntity<>(HttpMethod.GET, url.toUri()),
				new ParameterizedTypeReference<ErrorResponse>() {
				});
		ErrorResponse question = exchange.getBody();
		logger.debug("received response from :" + exchange );
		return question;
	}

	protected UserAnswer createUnknownQuestion(Game game,String userName) {
		List<Integer> questionsIds = game.getQuestionsIds();
		int QuestionIdNotExsiting = 0;
		UserAnswer userAnswer = new UserAnswer();
		boolean foundQuestionIdNotInGame = false;
		while (!foundQuestionIdNotInGame){
			QuestionIdNotExsiting = randomInt();
			if(questionsIds.contains(QuestionIdNotExsiting)){
				foundQuestionIdNotInGame = false;
			}
			else {
				foundQuestionIdNotInGame = true;
			}
		}
		userAnswer.setUserName(userName);
		userAnswer.setQuestionId(QuestionIdNotExsiting);
		userAnswer.setAnswerId(QuestionIdNotExsiting); // random answer the question not exist
		userAnswer.setGameId(game.getId());
		return userAnswer;
	}

	protected UserAnswer createWrongAnswer(Game game, String userName) {
		List<Integer> questionsIds = game.getQuestionsIds();
		UserAnswer userAnswer = new UserAnswer();
		Question question = getQuestion(questionsIds.get(0));
		userAnswer.setUserName(userName);
		userAnswer.setQuestionId(questionsIds.get(0));
		//choose the first incorrect answer
		userAnswer.setAnswerId(question.getIncorrectAnswer().get(0).getId());
		userAnswer.setGameId(game.getId());
		return userAnswer;
	}

	protected UserAnswer createCorrectAnswer(Game game,String UserName) {
		List<Integer> questionsIds = game.getQuestionsIds();
		UserAnswer userAnswer = new UserAnswer();
		Question question = getQuestion(questionsIds.get(0));
		userAnswer.setUserName(UserName);
		userAnswer.setQuestionId(questionsIds.get(0));
		userAnswer.setAnswerId(question.getCorrectAnswers().getId());
		userAnswer.setGameId(game.getId());
		return userAnswer;
	}

	protected int randomInt(){
		Random random = new Random();
		return random.ints(1000000,9999999).findFirst().getAsInt();
	}


}
