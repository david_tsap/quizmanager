package com.quiz.quizmanager;

import com.quiz.quizmanager.constant.StatusAnswer;
import com.quiz.quizmanager.exception.ErrorResponse;
import com.quiz.quizmanager.exception.Errors;
import com.quiz.quizmanager.viewmodel.Game;
import com.quiz.quizmanager.servicemodel.Question;
import com.quiz.quizmanager.viewmodel.UserAnswer;
import com.quiz.quizmanager.viewmodel.UserResponseAnswer;
import org.junit.Test;

public class QuestionTest extends QuizManagerBaseTests{

  @Test
  public void getQuestion_QuestionNotExist(){
    // create game
    Game game = createGame();
    int questionId = getNotExistQuestion(game);
    //
    ErrorResponse question = getQuestionError(questionId);
    assertEquals("error code should be QUIZ_10002, Question not found", Errors.QUIZ_10002.code(),question.getCode());
  }

  @Test
  public void validate_UserAddedToQuestion(){
    Game game = createGame();
    String userName = "user1";
    //create correct Answer
    UserAnswer correctAnswer = createCorrectAnswer(game,userName);
    UserResponseAnswer answer = answer(correctAnswer);
    assertEquals(answer.getStatus(), StatusAnswer.CORRECT);
    Question question = getQuestion(correctAnswer.getQuestionId());
    assertTrue("User not added to question", question.getUserAnswer().contains(userName));
  }

  @Test
  public void validate_UserCantAnswerTheQuestionAgain(){
    Game game = createGame();
    String userName = "user1";
    //create correct Answer
    UserAnswer correctAnswer = createCorrectAnswer(game,userName);
    UserResponseAnswer answer = answer(correctAnswer);
    assertEquals(answer.getStatus(), StatusAnswer.CORRECT);
    Question question = getQuestion(correctAnswer.getQuestionId());
    assertTrue("User not added to question", question.getUserAnswer().contains(userName));
    ErrorResponse answerError = errorAnswer(correctAnswer);
    assertEquals("wrong error user answer twice" ,Errors.QUIZ_10004.code(),answerError.getCode());
  }

  private int getNotExistQuestion(Game game) {
    boolean foundId = false;
    while (!foundId){
      int id = randomInt();
      if(!game.getQuestionsIds().contains(id)){
        return id;
      }
    }
    return 0;
  }
}
