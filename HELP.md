# Getting Started

### General 
Quiz manager service. The service responsible to manage the games and their respective leaderboards
by collecting answers from users (players).
questions source (https://opentdb.com/).

### Api
* GET: /api/leaderboard/{gameId}  
Response:  
[
    {
        "name": "David",
        "score": 25
    }
]  
* POST: /api/creategame  
Body:  
{}  
Response:  
{
    "id": 6925113,
    "questionsIds": [
        4361133,
        5464119,
        2095033,
        7786884,
        5250500,
        3184112,
        9655161,
        5374893,
        8031452,
        6364727
    ],
    "userScore": {}
}  
* POST: /api/answer  
Body:  
{
    "userName":"David",
    "gameId" : 8175993,
    "answerId" : 8269189,
    "questionId": 7145620
}  
Response:  
{
    "status": "CORRECT",
    "pointsEarned": 25
}  

* GET /api/question/{questionId}  
Response:  
{
    "id": 4361133,
    "question": "In the 1988 film &quot;Akira&quot;, Tetsuo ends up destroying Tokyo.",
    "correctAnswers": {
        "answer": "True",
        "id": 7248622
    },
    "incorrectAnswer": [
        {
            "answer": "False",
            "id": 7345802
        }
    ],
    "points": 25,
    "userAnswer": [
       "David"
    ]
}  
* POST /api/questions  
Body:   
[
        1248524,
        1030319,
        1859445,
        1258824,
        2375315,
        1209611,
        4493043,
        2595337,
        1109257,
        6157981
]  
Response:  
[
    {
        "id": 1248524,
        "question": "In Marvel Comics, Taurus is the founder and leader of which criminal organization?",
        "correctAnswers": {
            "answer": "Zodiac",
            "id": 3220230
        },
        "incorrectAnswer": [
            {
                "answer": "Scorpio",
                "id": 9810989
            },
            {
                "answer": "Tiger Mafia",
                "id": 7586300
            },
            {
                "answer": "The Union",
                "id": 3382294
            }
        ],
        "points": 50,
        "userAnswer": null
    },
    {
        "id": 1030319,
        "question": "The Principality of Sealand is an unrecognized micronation off the coast of what country?",
        "correctAnswers": {
            "answer": "The United Kingdom",
            "id": 4795001
        },
        "incorrectAnswer": [
            {
                "answer": "Japan",
                "id": 4648890
            },
            {
                "answer": "Austrailia",
                "id": 3329884
            },
            {
                "answer": "Argentina",
                "id": 3111985
            }
        ],
        "points": 50,
        "userAnswer": null
    },  
    .  
    .  
### Errors 
QUIZ_10000 - "internal error"  
QUIZ_10001 - "Game not found"  
QUIZ_10002 - "Question not found"  
QUIZ_10003 - "Answer not found"  
QUIZ_10004 - "user already answer question";  


### Compile
In root directory run:  
mvn clean install

### Run
After running compile using maven, in target directory, "quizmanager-0.0.1-SNAPSHOT.jar" will
be created.
cd to target dir and run:  
java -jar quizmanager-0.0.1-SNAPSHOT.jar


